# Dominando ambos lados de la Fuerza #

## Para saber más

* [Artículo sobre Swift en GenBetaDev](http://www.genbetadev.com/desarrollo-aplicaciones-moviles/el-nuevo-lenguaje-de-apple-swift)
* [Webinar sobre Swift en Español, primera impresiones.](https://www.youtube.com/watch?v=pdk-q2ccm2c)
* [Webinar sobre Swift en Español: DSLs con Swift, Core Data con Swift, Scene Kit con Swift y más.](https://www.youtube.com/watch?v=EcDE7x_s4iU)
* [Curso de Swift en Agbo](http://agbo.biz/blog/nuevo-curso-agbo-swift/)