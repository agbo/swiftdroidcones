// Playground - noun: a place where people can play

import UIKit


// Variables, constantes y tipado estrict
var sos: String = "¡Estoy rodeado de androideros!"
var mayday = "¡Estoy rodeado de androideros!"


var errorValue: Int

let error = 404

// Todo es un objeto, incluso los números
var age:Int

// -----------------------------------------------------

// Colecciones
// Arrays
var list = [1,2,3]

// Dicts
var bso = ["Yamamoto" : "Black Rain",
    "John Barry" : ["Dancing with wolves",
        "Born Free",
        "Out of Africa"]]
bso["John Barry"]


// ------------------------------------------------------

// Funciones
// También tienen tipos
func cube(x:Int) ->Int{
    
    return x*x*x
}

func square(x:Int) -> Int{
    return x*x
}

cube(2)
//cube(2.5)       // error!

// ---------------------------------------------------------

// Funciones y parámetros (mostrar las distintas formas de pasar argumentos)
// Nombre internos y externos
func greet(#name: String, #surname: String)->String{
    
    return name + " " + surname + "!"
}

var swift = greet(name: "Hi", surname: "Swift")

// Las funciones pueden devolver varios valores (explicar tuplas)
func getHTTPError()->(code:Int, description:String){
    
    return (404, "These are not the droids you're looking for!");
}

var rc = getHTTPError()
var (errorCode, desc) = getHTTPError()
var (_,description) = getHTTPError()

// --------------------------------------------------------

// Valores opcionales
var name:String?

func hello(name:String){
    println("Hello \(name)")
}

//hello(name!)    // explicar el operador "por cojones" -> esta linea causaría un error

// --------------------------------------------------------


// Funciones de Alto Nivel: funciones que devuelven funciones
// Sintaxis abreviada para funciones
func makeAdderFunction(a:Int) -> (Int)->(Int){
    
    //return {x in return x + a} // explicar la captura de entorno léxico
    return {a + $0}
}

var oneAdder = makeAdderFunction(1)
oneAdder(41)


// Funciones que aceptan funciones como parámetros
// Hacerlo primero sin typeAlias
typealias IntTransformer = (Int)->Int
func firstTen(fn:IntTransformer)->Int[]{
    
    var result:Int[] = []
    for each in 1...10{
        result.append(fn(each))
    }
    return result
}

var cubes = firstTen(cube)
var plus3 = firstTen({$0 + 3})

//----------------------------------------------------------

// Funciones genéricas
// >> Luego

// ---------------------------------------------------------


// Clases
class Note {
    var name = "New note"
    var text = "Type something"
    let creationDate = NSDate.date()
    
    // Los iniicalizadores se llaman sin el nombre, por eso el
    // nombre de los parámetros es muy importante: hay que ponerlo al llamar
    // Todos los parámetros de un init tienen por defecto el
    // nombre interno y externo (como si hubieses puesto # delante)
    init(text:String){
        self.text = text
        self.name = ""
    }
    
    init(text:String, name:String){
        self.text = text
        self.name = name
    }
    
    // Métodos
    func load(contentsOfFile path:String){
        var url = NSURL(string:path)
        self.text = NSString(contentsOfURL:url)
    }
    
    
    
}

var aNote = Note(text:"Lucas")
var otherNote = Note(text:"Hola", name:"una nota")

otherNote.load(contentsOfFile:"/Users/fernando/.bashrc")

// Si defines valores por defecto para cada una de tus propiedades
// y no aportas ningún init,
// Swift proporciona un inicializador por defecto que aplica dichos valores
class ToDoItem{
    var name = "Llevar la Luz de Jobs a los infieles"
    let creationDate = NSDate.date()
}

var charla = ToDoItem()











